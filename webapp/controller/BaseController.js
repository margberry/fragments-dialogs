sap.ui.define([
  "sap/ui/core/mvc/Controller"
], function (Controller) {
  "use strict";

  return Controller.extend("tutorial.controller.BaseController", {
    openInfoDialog() {
      if (!this.dialog) {
        this.dialog = this.loadFragment({
          name: "tutorial.view.InfoDialog",
        })
      }
      this.dialog.then(function (oDialogExecution) {
        this.infoDialog = oDialogExecution;
        this.infoDialog.open();
      }.bind(this));

    },
    onCloseInfoDialog() {
      this.infoDialog.close();
    }
  })
})
