sap.ui.define([
  "./BaseController",
  "sap/m/Dialog",
  "sap/m/Button",
  "sap/m/Text"
], function (BaseController, Dialog, Button, Text) {
  "use strict";

  return BaseController.extend("tutorial.controller.Products", {

    onConfirmBuy() {
      if (!this.buyDialog) {
        this.buyDialog = new Dialog({
          title: "Confirm purchase",
          content: new Text({
            text: "Are you sure you want to buy?"
          }),
          beginButton: new Button({
            text: "OK",
            press: function () {
              this.buyDialog.close();
            }.bind(this)
          }),
          endButton: new Button({
            text: "Close",
            press: function () {
              this.buyDialog.close();
            }.bind(this)
          })
        });
        
        // to get access to the controller's model
        this.getView().addDependent(this.buyDialog);
      }

      this.buyDialog.open();
    },
  })
})
